=============================
Readme
=============================

Threshold optimization tool, that enables developer to get the most accurate threshold for the given models outputs.

Build
-----

1. Install poetry following official `installation guide <https://python-poetry.org/docs/#installation>`_:

.. note::
	This step will install Poetry system-wide so you do not need to run it for each new project.

2. Build optimizer package using poetry

.. code-block:: bash

	cd <my_project>
	poetry build


.. note::
	Package wheels will be saved to dist directory, they can be uploaded to pypi later on, using twine for example (or other tools, manual upload)


Installation
------------

Installation of threshold_optimizer involves the following steps:

1. Install poetry following official `installation guide <https://python-poetry.org/docs/#installation>`_:

.. note::
	This step will install Poetry system-wide so you do not need to run it for each new project.


2. Create a new project directory

.. code-block:: bash

	mkdir <my_project>
	cd <my_project>


3. Create a virtual environment for the project using your favorite environment manager. For example, using ``venv``:

.. note::
	Omitting this step may break some other tools (like jupyterHub or Spyder on Windows anaconda installation)

.. code-block:: bash

	python -m venv venv
	source venv/bin/activate # or venv\Scripts\activate - on Windows

4. Install threshold_optimizer into your virtual environment

.. code-block:: bash

	pip install threshold_optimizer


Usage
-----

.. code-block:: python

	from threshold_optimizer import optimize_threshold
	optimize_threshold(scores, classes)


