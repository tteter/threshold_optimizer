from typing import List, Dict


def is_the_sample_classified_correctly(score: float, actual_class: int, threshold: float) -> bool:
    """
    Checks if the sample has been classified correctly.

    :param score: score value
    :param actual_class: class value (1 or -1)
    :param threshold: currently checked threshold value
    :return: boolean indicating if the sample has been classified correctly
    """
    return actual_class == 1 if score > threshold else actual_class == -1


def optimize_threshold(scores: List[float], classes: List[int]) -> float:
    """
    Looks for the threshold with the highest fraction of correctly classified samples.

    :param scores:
    :param classes:
    :return:
    """
    thresholds_accuracy_map: Dict[float, int] = {}
    for sample_threshold in scores:
        thresholds_accuracy_map[sample_threshold] = sum(
            is_the_sample_classified_correctly(score, actual_class, sample_threshold)
            for score, actual_class in zip(scores, classes))
    return max(thresholds_accuracy_map.items(), key=lambda item: item[1])[0]
