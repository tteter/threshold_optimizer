test:  ## run tests quickly with the default Python
    pytest tests/

docs: ## generate Sphinx HTML documentation
	rm -rf docs/*
	sphinx-apidoc -o docs/ threshold_optimizer

release: ## package and upload a release
	twine upload dist/*

dist: ## builds source and wheel package
	poetry build

install: ## install the package to the active Python's site-packages
	poetry install
