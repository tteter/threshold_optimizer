import pytest

from threshold_optimizer.threshold_optimizer import optimize_threshold


@pytest.mark.parametrize(
        "scores, actual_classes, expected_threshold",
        [
            ([0.1, 0.3, 0.4, 0.7], [-1, -1, 1, 1], 0.3),
            ([0.1, 0.4, 0.3, 0.7], [-1, 1, -1, 1], 0.3),
        ],
    )
def test_optimization(scores, actual_classes, expected_threshold):
    threshold = optimize_threshold(scores, actual_classes)
    assert expected_threshold == threshold, f"""Expected threshold: {expected_threshold}, actual: {threshold}"""
